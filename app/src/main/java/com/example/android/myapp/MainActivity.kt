package com.example.android.myapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val persons = ArrayList<Person>().apply {
            add(Person("Marcos Luna", 18, true))
            add(Person("Johan Luna", 30, false))
            add(Person("Messi Qa", 40, true))
            add(Person("Lidy Luna", 18, true))
            add(Person("Mari Luna", 18, true))
            add(Person("Jose Luna", 18, true))
            add(Person("Marcos Luna", 18, true))
            add(Person("Marcos Luna", 18, true))
            add(Person("Marcos Luna", 18, true))
            add(Person("Marcos Luna", 18, true))
            add(Person("Marcos Luna", 18, true))
            add(Person("Marcos Luna", 18, true))
            add(Person("Marcos Luna", 18, true))
            add(Person("Marcos Luna", 18, true))
            add(Person("Marcos Luna", 18, true))
            add(Person("Marcos Luna", 18, true))
        }

        findViewById<RecyclerView>(R.id.rvPerson).apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = PersonAdapter(persons, this@MainActivity)
        }


        Observable.just(1, 2, 3, 4, 5)
                .flatMap {
                    if (it == 3) {
                        Observable.error(Exception())
                    } else {
                        Observable.just(it)
                    }
                }.retryWhen {
                    it.zipWith(Observable.range(1, 5), BiFunction<Throwable, Int, Observable<Long>> {
                                _, _ -> Observable.timer(500, TimeUnit.MILLISECONDS) })
                }.subscribe {
                    Log.d("GABRIEL", "$it")
                }


    }
}
