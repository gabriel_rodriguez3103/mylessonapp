package com.example.android.myapp

data class Person(
        val name: String,
        val age: Int,
        val isMarried: Boolean)